package com.example.phonebookdemokt.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.phonebookdemokt.R
import com.example.phonebookdemokt.adapter.ContactAdapter
import com.example.phonebookdemokt.view_model.ViewModelContact
import com.google.android.material.floatingactionbutton.FloatingActionButton

class AllContactsFragment : Fragment() {

    private var recyclerView: RecyclerView? = null
    private var loadingBarContainer: FrameLayout? = null
    private var viewModelContact: ViewModelContact? = null
    private var fbtnAdd: FloatingActionButton? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_all_contacts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViewsById(view)
        setupWithViewModel()
    }

    private fun findViewsById(view: View) {
        recyclerView = view.findViewById(R.id.general_contact_cardview_all_contacts)
        loadingBarContainer = view.findViewById(R.id.progress_bar_container_all_contacts)
    }

    private fun setupWithViewModel() {
        recyclerView!!.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        viewModelContact = ViewModelProviders.of(this).get(ViewModelContact::class.java)


        viewModelContact!!.getAllContacts().observe(activity!!,
            Observer { contacts ->
                val adapter = ContactAdapter(requireActivity())
                adapter!!.setContactList(contacts)
                recyclerView!!.adapter = adapter
            })
    }


}
