package com.example.phonebookdemokt.adapter

import com.example.phonebookdemokt.model.Contact


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.example.phonebookdemokt.databinding.ItemContactBinding


import java.util.ArrayList


class ContactAdapter(private val context: Context) : RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {

    private val inflater: LayoutInflater
    private val contacts: MutableList<Contact>


    init {
        this.contacts = ArrayList()
        inflater = LayoutInflater.from(context)
    }

    fun setContactList(contactList: List<Contact>?) {
        this.contacts.clear()
        if (contactList != null) {
            this.contacts.addAll(contactList)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactAdapter.ContactViewHolder {
        val binding = ItemContactBinding.inflate(inflater, parent, false)
        return ContactViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ContactAdapter.ContactViewHolder, position: Int) {
        holder.binding.setContact(contacts[position])
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    fun getContactAt(position: Int): Contact {
        return contacts[position]
    }

    class ContactViewHolder(internal var binding: ItemContactBinding) : RecyclerView.ViewHolder(binding.getRoot())
}