package com.example.phonebookdemokt.model

import com.google.gson.annotations.SerializedName

public class Contact {
    @SerializedName("firstName")
     var firstName: String

    @SerializedName("lastName")
     var lastName: String

    @SerializedName("phone")
     var phone: String

    @SerializedName("mail")
     var mail: String

    @SerializedName("notes")
     var notes: String

    public constructor(
        firstName: String,
        lastName: String,
        phone: String,
        mail: String,
        notes: String
    ) {
        this.firstName = firstName
        this.lastName = lastName
        this.phone = phone
        this.mail = mail
        this.notes = notes
    }



}





