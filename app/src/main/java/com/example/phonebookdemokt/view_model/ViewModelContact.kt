package com.example.phonebookdemokt.view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.phonebookdemokt.model.Contact
import com.example.phonebookdemokt.repository.ContactRepo

class ViewModelContact : ViewModel() {

    private val contactRepo: ContactRepo = ContactRepo.getInstance
//    private val API_KEY: String = "a5b39dedacbffd95e1421020dae7c8b5ac3cc"

    public fun getAllContacts() : MutableLiveData<List<Contact>>{
        return contactRepo.getContacts("a5b39dedacbffd95e1421020dae7c8b5ac3cc")
    }


}