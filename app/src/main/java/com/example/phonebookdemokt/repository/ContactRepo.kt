package com.example.phonebookdemokt.repository

import androidx.lifecycle.MutableLiveData
import com.example.phonebookdemokt.model.Contact
import com.example.phonebookdemokt.web_service.ContactWebService
import com.example.phonebookdemokt.web_service.RetrofitNetworkService
import retrofit2.Call
import retrofit2.Response
import retrofit2.Callback



class ContactRepo {

    private val contactWebService: ContactWebService


    private constructor() {
        contactWebService = RetrofitNetworkService.createService()
    }

    public companion object {
        val getInstance = ContactRepo()
    }

    fun getContacts(apiKey: String): MutableLiveData<List<Contact>> {

        val allContacts = MutableLiveData<List<Contact>>()

        contactWebService.getContacts(apiKey).enqueue(object : Callback<List<Contact>> {
            override fun onFailure(call: Call<List<Contact>>, t: Throwable) {
                t.printStackTrace()
                allContacts.postValue(null)
            }

            override fun onResponse(call: Call<List<Contact>>, response: Response<List<Contact>>) {
                if (response.isSuccessful) {
                    allContacts.postValue(response.body())
                }
            }
        })
        return allContacts
    }

}