package com.example.phonebookdemokt.web_service

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitNetworkService {

//    private val BASE_URL: String = "a5b39dedacbffd95e1421020dae7c8b5ac3cc"

    companion object {
        fun createService(): ContactWebService {
            return Retrofit.Builder()
                .baseUrl("https://stdevtask3-0510.restdb.io/rest/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ContactWebService::class.java)
        }
    }
}


//    private val retrofit = Retrofit.Builder()
//        .baseUrl(BASE_URL)
//        .addConverterFactory(GsonConverterFactory.create())
//        .build()
//        .create(ContactWebService::class.java)

//    val BASE_URL: String = "https
//    ://stdevtask3-0510.restdb.io/rest/"