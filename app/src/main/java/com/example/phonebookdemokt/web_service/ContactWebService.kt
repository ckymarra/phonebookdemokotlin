package com.example.phonebookdemokt.web_service

import com.example.phonebookdemokt.model.Contact
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header

interface ContactWebService {

    @GET("contacts")
    fun getContacts(@Header("apiKey") apiKey: String): Call<List<Contact>>

}