package com.example.phonebookdemokt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.phonebookdemokt.fragment.AllContactsFragment

class MainActivity : AppCompatActivity() {

    private var fragment: AllContactsFragment = AllContactsFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        changeFragment(fragment, null)

    }

    private fun changeFragment(fragment: Fragment, name: String?) {
        if (name == null) {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.addToBackStack(null)
            transaction.replace(R.id.frame_container, fragment).commit()
        } else {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.addToBackStack(name)
            transaction.replace(R.id.frame_container, fragment).commit()
        }
    }


}
