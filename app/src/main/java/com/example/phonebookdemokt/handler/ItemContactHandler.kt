package com.example.phonebookdemokt.handler

import android.view.View

interface ItemContactHandler {
    fun onItemContactClicked(view: View)
}